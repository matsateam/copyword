package net.ukr.wumf;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;

public class FileOperation {
	public static String readFile(File file) {
		String str = "";
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String s;
			while ((s = br.readLine()) != null) {
				str += s + " ";
			}
		} catch (IOException ex) {

			System.out.println(ex.getMessage());
		}
		return str;
	}

	private static String findCommonWords(String[] files1, String[] files2) {
		String strForNewFile = "";
		for (String string : files1) {
			for (String string2 : files2) {
				if ((string.compareTo(string2)) == 0) {
					strForNewFile += string + " ";
				}
			}
		}
		return strForNewFile;
	}

	public static void mergeFiles(File file1, File file2, File file3) {
		String[] strArr1 = FileOperation.readFile(file1).split(" ");
		String[] strArr2 = FileOperation.readFile(file2).split(" ");
		String str = findCommonWords(strArr1, strArr2);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file3))) {
			bw.write(str);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

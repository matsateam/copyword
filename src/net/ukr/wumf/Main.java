package net.ukr.wumf;

import java.io.*;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		File file1 = new File("file1.txt");
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("file1.txt", true)));
			out.println("What is you name");
			out.close();
		} catch (IOException e) {
			System.out.println(e);
		}
		File file2 = new File("file2.txt");
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("file2.txt", true)));
			out.println("Hello my name is Pedro Depacos");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		File file3 = new File("file3.txt");

		FileOperation.mergeFiles(file1, file2, file3);

		file1.delete();
		file2.delete();

	}
}
